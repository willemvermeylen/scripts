#! /bin/bash

PATH=/usr/gnu/bin:/usr/bin:/usr/sbin:/sbin:/opt/csw/bin:/usr/local/bin:/bin

TYPE=$(sudo /opt/csw/sbin/smartctl -H -A /dev/rdsk/"$(echo | sudo format | awk '/[[:space:]]+c[0-9]+(d|t)[0-9]+/ {print $2}' | egrep '^.{8,25}' | head -n 5 | tail -1)" -i -d sat | grep "Device Model:" | awk '{print $3}')
echo "$TYPE";

if [ $TYPE = "INTEL" ];
then
        for disk in $(echo | sudo format | awk '/[[:space:]]+c[0-9]+(d|t)[0-9]+/ {print $2}' | egrep '^.{8,25}'); do
        sudo /opt/csw/sbin/smartctl -H -A /dev/rdsk/"$disk" -d sat | grep Media_Wearout_Indicator | awk '$4~/^00.$/  {print $4}'
        done
elif [ ${TYPE} = "TOSHIBA" ];
then
        for disk in $(echo | sudo format | awk '/[[:space:]]+c[0-9]+(d|t)[0-9]+/ {print $2}' | egrep '^.{8,25}'); do
        sudo /opt/csw/sbin/smartctl -H -A /dev/rdsk/"$disk" -d sat | grep Total_LBAs_Written | awk '$4~/^00.$/  {print $4}'
        done
else
        for disk in $(echo | sudo format | awk '/[[:space:]]+c[0-9]+(d|t)[0-9]+/ {print $2}' | egrep '^.{8,25}'); do
        sudo /opt/csw/sbin/smartctl -H -A /dev/rdsk/"$disk" -d sat | grep WearLevelingCount | awk '$4~/^00.$/  {print $4}'
        done
fi

