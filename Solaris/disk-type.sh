#! /bin/bash

for output in $(echo | sudo format | awk '/[[:space:]]+c[0-9]+(d|t)[0-9]+/ {print $2}' | egrep '^.{8,25}')

do sudo /opt/csw/sbin/smartctl -i -d sat /dev/rdsk/"$output" | grep "Device Model:" | awk '{print $4}'

done

for output in $(echo | sudo format | awk '/[[:space:]]+c[0-9]+(d|t)[0-9]+/ {print $2}' | egrep '^.{8,25}')

do sudo /opt/csw/sbin/smartctl -i -d scsi /dev/rdsk/"$output" | grep "Product:" | awk '{print $2}'

done
