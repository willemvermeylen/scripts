#! /bin/bash

for disk in $(echo | sudo format | awk '/[[:space:]]+c[0-9]+(d|t)[0-9]+/ {print $2}' | egrep '^.{8,25}'); do
sudo /opt/csw/sbin/smartctl -H -A /dev/rdsk/"$disk" -d sat | grep Media_Wearout_Indicator | awk '$4~/^00.$/  {print $4}'

done
