#!/bin/bash
ansible='/usr/bin/ansible'
password='Nightkorn1983'
get_spare() {
        hostname=$1 ansible=$2 passwd=$3
        $ansible all -i "$hostname," -a "sudo smartctl -i /dev/\$(sudo zpool status |grep spare -A1 |grep -v spares |awk '{print \$1}') | grep 'Model:\|Product:' | awk '{print \$2\$3\$4}' | sed -r 's/Model://'| sed -r 's/HGST//'" -m raw -b --extra-vars "ansible_sudo_pass=$passwd" | tee -a ./output/${hostname}_spare.txt 
	#sudo geom disk list |grep $(sudo zpool status |grep spare -A1 |grep -v spares |awk '{print $1}') -A4 |grep descr: |awk '{print $3}'
	#sudo smartctl -i /dev/$(sudo zpool status |grep spare -A1 |grep -v spares |awk '{print $1}') | grep "Device Model" | awk '{print $4}'
}

get_mostdisk() {
        hostname=$1 ansible=$2 passwd=$3
	#$ansible all -i "$hostname," -a "/usr/local/bin/bash -c 'for output in \$(for i in {0..35}; do sudo smartctl -i /dev/da$i | grep 'Model:\|Product:' | awk '{print $2$3$4}' | sed -r 's/Model://'| sed -r 's/HGST//'; done); do echo $output; done | sort | uniq -c |sort -r'" -m raw -b --extra-vars "ansible_sudo_pass=$passwd"  | tee -a ./output/${hostname}_disks.txt
        $ansible all -i "$hostname," -a "/usr/local/bin/bash -c 'for i in {0..35}; do sudo smartctl -i /dev/da\$i | (grep -e Product: -e Model:); done '" -m raw -b --extra-vars "ansible_sudo_pass=$passwd"  | tee -a ./output/${hostname}_disks.txt
	#for output in $(for i in {0..35}; do sudo smartctl -i /dev/da$i | grep 'Model:\|Product:' | awk '{print $2$3$4}' | sed -r 's/Model://'| sed -r 's/HGST//'; done); do echo $output; done | sort | uniq -c |sort -r
	#sudo geom disk list |grep descr |sort | uniq -c |sort -r |awk '{print $4}' |head -n1
}

get_hosts() {
        cat /home/wvermeylen/hddstorage.txt
}

execute() {
        hostname=$1 ansible=$2 passwd=$3
	$ansible all -i "$hostname," -a "/home/wvermeylen/checkspare.sh" -m raw -b --extra-vars "ansible_sudo_pass=$passwd"  | tee -a ./output2.txt
}

remove() {
        hostname=$1 ansible=$2 passwd=$3
        $ansible all -i "$hostname," -a "rm /home/wvermeylen/checkspare.sh" -m raw -b --extra-vars "ansible_sudo_pass=$passwd"  
}

execute_sun() {
        hostname=$1 ansible=$2 passwd=$3
        $ansible all -i "$hostname," -a "/export/home/wvermeylen/checkspare.sh" -m raw -b --extra-vars "ansible_sudo_pass=$passwd"  | tee -a ./output2.txt
}

remove_sun() {
        hostname=$1 ansible=$2 passwd=$3
        $ansible all -i "$hostname," -a "rm /export/home/wvermeylen/checkspare.sh" -m raw -b --extra-vars "ansible_sudo_pass=$passwd"  
}

uname() {
        hostname=$1 ansible=$2 
        $ansible all -i "$hostname," -a "uname" -m raw |grep -v $hostname > ./$hostname.txt
}

#stty -echo
#read -p "Enter your SUDO password: " passwd; echo
#read -p "Enter your SUDO password again: " passwd2; echo
#stty echo
#[ $passwd = $passwd2 ] || { echo 'Passwords are not equal, stopping script...' ; exit 1; }
rm ./output2.txt > /dev/null 2>&1

for host in $(get_hosts); do
	#get_spare ${host} ${ansible} ${password}
	#get_mostdisk ${host} ${ansible} ${password}
	compare="SunOS"
	uname ${host} ${ansible}
	for remoteos in $(cat ./${host}.txt); do
		if [[ "$remoteos" == "SunOS"* ]]; then
        		scp -q ./checkspare.sh ${host}:/export/home/wvermeylen/
        		execute_sun ${host} ${ansible} ${password}
        		remove_sun ${host} ${ansible} ${password}
		else
        		scp -q ./checkspare.sh ${host}:/home/wvermeylen/
        		execute ${host} ${ansible} ${password}
        		remove ${host} ${ansible} ${password}
		fi
		rm ./${host}.txt > /dev/null 2>&1
	done
done
