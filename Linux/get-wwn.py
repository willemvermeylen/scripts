#!/usr/bin/env python3
"""
    This script gets the wwn of a given disk
"""
import subprocess
import sys
import argparse
import re


def parsed_disklogger(args: str):
    """
        Function that rusn disklogger as root
        and parses it to a Python object
    """
    import os
    import json

    uid = os.getuid()
    data = ""

    if (uid):
        data = subprocess.run(
            "sudo disklogger {0}".format(args),
            shell=True,
            capture_output=True
        ).stdout
    else:
        data = subprocess.run(
            "disklogger {0}".format(args),
            shell=True,
            capture_output=True
        ).stdout

    if data:
        return json.loads(data)
    else:
        return dict()


def sizeof_fmt(num, suffix="B"):
    for unit in ["", "Ki", "Mi", "Gi", "Ti", "Pi", "Ei", "Zi"]:
        if abs(num) < 1024.0:
            return f"{num:3.1f}{unit}{suffix}"
        num /= 1024.0
    return f"{num:.1f}Yi{suffix}"

osdisk1 = subprocess.run("cat /proc/mdstat | egrep sd[a-z]+ | awk '{print $5}' | sed 's/[0-9]\[[0-9]\]//g'",shell=True,capture_output=True).stdout
osdisk2 = subprocess.run("cat /proc/mdstat | egrep sd[a-z]+ | awk '{print $6}' | sed 's/[0-9]\[[0-9]\]//g'",shell=True,capture_output=True).stdout
osdisk1_decoded = osdisk1.decode('utf-8').strip()
osdisk2_decoded = osdisk2.decode('utf-8').strip()
osdisks = [osdisk1_decoded, osdisk2_decoded] 


def filter_out_virtual_disks(data):
    data_filtered = []
    for disk in data:
        if disk["model"] != "SMINST":
            data_filtered.append(disk)
    return data_filtered


if __name__ == "__main__":
    # Parse the arguments that are given
    parser = argparse.ArgumentParser(description='Script for using disklogger / getting WWNs and SNs.')
    parser.add_argument('disk', type=str, nargs='*', help='Disk(s) to lookup (either, wwn, serial or device)')
    parser.add_argument('-f', dest='full', action='store_true', help='List all disks')
    parser.add_argument('-s', dest='short', action='store_true', help='Print short WWN (last 8 chars)')
    parser.add_argument('-S', dest='serial', action='store_true', help='Print Serial Number as well')
    parser.add_argument('-z', dest='zpool', action='store_true', help='Print only zpool-disks')
    parser.add_argument('-t', dest='disktype', action='store_true', help='Print disk type and size')
    parser.add_argument('-o', dest='os-disks', action='store_true', help='Print only os disks')
    parser.add_argument('-n', dest='no-collect', action='store_true', help='Skip disklogger collect')
    arguments = vars(parser.parse_args())

    # Rewrite arguments to local variables
    disks = arguments.get('disk')
    short = arguments.get('short')
    full = arguments.get('full')
    serial = arguments.get('serial')
    zpool = arguments.get('zpool')
    disktype = arguments.get('disktype')
    os_exclude = arguments.get('os-disks')
    no_collect = arguments.get('no-collect')

    # Print help if command is not populated
    if (not len(disks) and not full and not os_exclude):
        parser.print_help()
        exit(0)

    # Run disklogger collect to ensure that we have data
    # and dump the data into JSON format
    if not no_collect:
        parsed_disklogger('collect')
    data = parsed_disklogger('dump')
    sorted_disks = arguments.get('disk')

    data = filter_out_virtual_disks(data)

    if (full):
        disks = [disk.get('device') for disk in data]
        sorted_on_len = sorted(disks) 
        sorted_disks = sorted(sorted_on_len, key=len)

    if (os_exclude):
        disks = [osdisks.get('device') for osdisks in data]
        sorted_on_len = sorted(disks)
        sorted_disks = sorted(sorted_on_len, key=len)
        disks_to_remove = sorted_disks[:]
        for disk in osdisks:
            disks_to_remove.remove(disk)
        
        sorted_disks = [x for x in sorted_disks if x not in disks_to_remove]

    populated = {}

    r1 = re.compile(r"nvme[0-9]+$")
    test_str = ' '.join([str(item) for item in sorted_disks])
    subst = test_str + "n1"
    
    sorted_disks = [re.sub(r1, subst, i) for i in sorted_disks]

    for disk in sorted_disks:
        _found = [
            x for x in data if
            x.get('device') == disk or
            x.get('wwn') == disk or
            x.get('serial') == disk
        ]

        if len(_found):
            result = _found[0]
            if re.search('cd|dvd|virtual', result.get('model'), re.IGNORECASE) or not result.get('present'):
                continue
            if not zpool or (zpool and result.get('type') == 'zpool'):
                _device = result.get('device')
                _wwn    = result.get('wwn')
                final_wwn = ''
                if _wwn:
                    final_wwn = _wwn[-8:].upper() if short else 'wwn-0x' + _wwn
                _serial = "\t[{}]".format(result.get('serial')) if serial else ''
                _desc   = "\t({}-{})".format(result.get('model'), sizeof_fmt(result.get('size'))) if disktype else ''
                populated[disk] = '{0}\t{1}{2}{3}'.format(
                    _device,
                    final_wwn,
                    _serial,
                    _desc
                )
            
        else:
            populated[disk] = 'ERROR: disk {0} not found in disklogger dump'
    

    for k, v in populated.items():
        print(v)
