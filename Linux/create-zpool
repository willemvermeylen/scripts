#!/bin/bash

set -e

HOSTNAME=$(hostname -f)
NVME=$(nvme list | awk 'NR>=3')

case $HOSTNAME in
  bu*.spod*.*.transip.io)
    SERVER_TYPE="BACKUP";;
  bu*.pod*.*.transip.io)
    SERVER_TYPE="BACKUP";;
  st*.spod*.*.transip.io)
    SERVER_TYPE="VOLUME";;
  st*.*.transip.io)
    if [ -z "$NVME" ]; then
      SERVER_TYPE="SSD";
    else
      SERVER_TYPE="NVME";
    fi
esac

if [ -z "${SERVER_TYPE}" ]; then
  echo "Unable to determine server type for ${HOSTNAME}"
  exit 2
fi

# check for zpools
echo "Checking if we need to create the zpool"

ZPOOL_NAME=${HOSTNAME/.*/}
if zpool list ${ZPOOL_NAME} >/dev/null 2>&1; then
    echo -e "\tWe don't, ${ZPOOL_NAME} already exists"
    exit
fi

# figure out which disks are part of the OS raid
IGNORED_DISKS=$(grep -Eo sd. /proc/mdstat | paste - - | sed -e 's/^/\\</' -e 's/$/\\>/' -e 's/\t/\\>|\\</')

OLDIFS="${IFS}"
IFS=$'\n'
DISKS=($(lsblk -d -o name,wwn,size -n | grep -v -E ${IGNORED_DISKS} | sort -n -k1.5))
IFS="${OLDIFS}"

if [ -z "${DISKS}" ]; then
  echo -e "\tError: no disks could be found to create the zpool"
  exit 2
fi

echo -e "\tWe do"

COMMAND="zpool create -f ${ZPOOL_NAME}"

TOTAL_DISKS=${#DISKS[@]}

NUM_DISK_PER_RAIDZ=0
case ${SERVER_TYPE} in
  BACKUP|VOLUME)
    RAID_TYPE="raidz2"
    case ${TOTAL_DISKS} in
      36) NUM_DISK_PER_RAIDZ=5;;
      24) NUM_DISK_PER_RAIDZ=6;;
    esac;;
  SSD)
    RAID_TYPE="raidz2"
    case ${TOTAL_DISKS} in
      24) NUM_DISK_PER_RAIDZ=8;;
      22) NUM_DISK_PER_RAIDZ=7;;
        esac;;
  NVME)
    RAID_TYPE="raidz3"
    case ${TOTAL_DISKS} in
      12) NUM_DISK_PER_RAIDZ=11;;
    esac;;
  *)
    echo -e "\tUnhandled server type ${SERVER_TYPE}"
    echo -e "\tAborting"
    exit 2
    ;;
esac

if [ ${NUM_DISK_PER_RAIDZ} -eq 0 ]; then
    echo -e "\tUnhandled disk count ${TOTAL_DISKS} for server type ${SERVER_TYPE}"
    echo -e "\tAborting"
    exit 2
fi

DISK_COUNT=0
DISKS_IN_RAIDZ=0

echo -e "\n\tzpool layout for ${ZPOOL_NAME} would be:\n"

for ITEM in "${DISKS[@]}"; do
  DISK_COUNT=$((DISK_COUNT + 1))
  DATA=($(echo ${ITEM}))

  DISK_SIZE=${DATA[2]}
  if [ -z "$NVME" ]; then 
    DISK="wwn-${DATA[1]}"
  else
    DISK="${DATA[0]}"
  fi

  if [ $DISK_COUNT -eq $TOTAL_DISKS ]; then
    COMMAND="${COMMAND} spare ${DISK}"
    echo -e "\tspare\n\t\t${DISK} (${DISK_SIZE})"
  else
    # Add it to a raidz
    if [ $DISKS_IN_RAIDZ -eq $NUM_DISK_PER_RAIDZ ]; then
      DISKS_IN_RAIDZ=0
    fi
    if [ $DISKS_IN_RAIDZ -eq 0 ]; then
      COMMAND="${COMMAND} ${RAID_TYPE}"
      echo -e "\t${RAID_TYPE}"
    fi
    COMMAND="${COMMAND} ${DISK}"
    echo -e "\t\t${DISK} (${DISK_SIZE})"
    DISKS_IN_RAIDZ=$(($DISKS_IN_RAIDZ + 1))
  fi
done

echo -n "Proceed? [Y/n] "
read -r CONFIRM
case ${CONFIRM} in
    [Nn]) exit;;
esac

eval $COMMAND

if [ $? -ne 0 ]; then
  echo "FAILED, please fix manually"
  exit 2
fi

zpool status
