#!/usr/bin/env python3
"""
    This script blinks the led on each disk
"""
import subprocess
import sys
import os
import json
from pprint import pprint


def sort_by(item):
    """
        Sub-function for sorting, using instead of Lambda
    """

    return (len(item['device']), item['device'])

def parsed_disklogger(args: str):
    """
        Function that run disklogger as root
        and parses it to a Python object
    """

    uid = os.getuid()
    data = ""

    if (uid):
        data = subprocess.run(
            "sudo disklogger {0}".format(args),
            shell=True,
            capture_output=True
        ).stdout
    else:
        data = subprocess.run(
            "disklogger {0}".format(args),
            shell=True,
            capture_output=True
        ).stdout

    if data:
        return json.loads(data)
    else:
        return dict()

if __name__ == "__main__":
    # Run disklogger collect to ensure that we have data
    # and dump the data into JSON format
    parsed_disklogger('collect')
    data = parsed_disklogger('dump')

    while True:

        # Check if we're on a Supermicro
        _sm = 'Supermicro' in subprocess.run('{0} ipmitool fru'.format(
            'sudo ' if os.getuid() else ''
        ), shell=True, capture_output=True, text=True).stdout

        # Get NVME drives
        _nvmedrives = {}
        if _sm:
            _nvmedrives = json.loads(subprocess.run('parsed-ipmicfg disks',
            shell=True, capture_output=True, text=True).stdout)

        cmd = input('Toggle [On / Off / Exit]? ').upper()
        
        #filter for all currently available disks and check if os disks are on the backplane or not by selecting physical locations and the present boolean is True

        filtereddata = sorted([selection for selection in data if selection['present'] and 'physical' in selection], key = sort_by)

        if cmd == 'ON':
            
            #enable diskleds with correct disk list

            for disk in filtereddata:
                parsed_disklogger('locate {0} on'.format(disk.get('device')))
                print('DISKLED ENABLING: {0}, {1}'.format(
                    disk.get('device'),
                    disk.get('wwn')
                ))
            print('\nALL DISKLEDS ENABLED! (This takes a few minutes on NVMe)\n')

        if cmd == 'OFF':
            
            #disable diskleds with correct disk list

            for disk in filtereddata:
                parsed_disklogger('locate {0} off'.format(disk.get('device')))
                print('DISKLED DISABLING: {0}, {1}'.format(
                    disk.get('device'),
                    disk.get('wwn')
                ))
            print('\nDISKLEDS DISABLED! (This takes a few minutes on NVMe)\n')

        if cmd == 'EXIT':

            print('\nBYE!')
            break
