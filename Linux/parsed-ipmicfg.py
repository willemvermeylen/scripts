#!/usr/bin/env python3
"""
    This script checks labels on ipmicfg-output
"""
import subprocess
import sys
import re

def check_ipmitool_board():
    """ 
        Function that checks if we're on a H11SSW
        board
    """
    import os
    _uid = os.getuid()

    if (_uid):
        return 'Supermicro' in str(subprocess.run(
            'sudo ipmitool fru print',
            shell=True,
            capture_output=True
        ).stdout)
    else:
        return 'Supermicro' in str(subprocess.run(
            'ipmitool fru print',
            shell=True,
            capture_output=True
        ).stdout)

def run_ipmicfg_command(args: str):
    """ 
        Function that runs ipmicfg-command as root 
        and returns the STDOUT
    """
    import os

    uid = os.getuid()

    if (uid):
        return subprocess.run(
            'sudo ipmicfg {0}'.format(args),
            shell=True,
            capture_output=True,
            text=True
        ).stdout
    else:
        return subprocess.run(
            'ipmicfg {0}'.format(args),
            shell=True,
            capture_output=True,
            text=True
        ).stdout
        
def get_and_parse_nvme_info():
    _stdout = run_ipmicfg_command('-nvme info')
    _AOC = None
    _GROUP = 0
    _LOCATE = None
    _SLOT = None
    _SERIAL = None
    
    disks = {}
    
    for line in _stdout.splitlines():
        if 'AOC' in line:
            _AOC = re.search('AOC Number: (.*)\] \[', line).group(1)
        if 'Slot' in line:
            _SLOT = re.search('\|[ ]*(.*)', line).group(1)
        if 'Serial' in line:
            _SERIAL = re.search('\|[ ]*(.*)', line).group(1)
        if 'Located' in line:
            _LOCATE = re.search('\|[ ]*(.*)', line).group(1)
        if 'End of Group' in line:
            _GROUP = int(re.search('\((.*)\)', line).group(1)) + 1
        if 'Max Power Requirement' in line:
            if _AOC and _SLOT and _SERIAL and _LOCATE:
                disks[_SERIAL] = {
                        'AOC': _AOC,
                        'SLOT': _SLOT,
                        'SERIAL': _SERIAL,
                        'GROUP': _GROUP,
                        'LOCATE': _LOCATE,
                    }
                _SLOT = None
                _SERIAL = None
                _LOCATE = None

    return disks
    
if __name__ == "__main__":
    import json
    # Check if we're on a Supermicro board
    if check_ipmitool_board():
        # Run nvme parsing to ensure that we have data
        # and dump the data into JSON format
        diskinfo = get_and_parse_nvme_info()
        arguments = sys.argv[1:]
        if 'disks' in arguments:
            print(
                json.dumps(diskinfo)
            )
    else:
        print('[ERROR] Running ipmicfg on a non-Supermicro board')
        exit(1)