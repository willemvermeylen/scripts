#! /bin/sh
#============================================================================
# HEADER
#============================================================================
# DESCRIPTION
#    This script fetches the WWN of disks
#
# OPTIONS
#       
#     -h:     Show this information
#     -f:     Show full disk name as used in zpool
#     -s:     Show last 8 characters (used as disk labels on the server)
#
#============================================================================
# IMPLEMENTATION
#    version         get-wwn 1.0.0
#    author          Willem Vermeylen
#
#============================================================================
#  HISTORY
#     2020/04/16 : wvermeylen : first initial release
# 
#============================================================================
# END_OF_HEADER
#============================================================================

name=$(basename "$0")

# help information

usage() {

cat << EOT

Error: arguments required
Usage: ${name} option

    Options:
        -h:     Show this information
        -f:     Show full disk name as used in zpool
        -s:     Show last 8 characters (used as disk labels on the server)

EOT
}

# checking if the script is run as root

check_root()
{
    myuid=$(id -u)
    if [ $myuid != '0' ]; then
        echo "This script requires root privilege"
        exit 1
    fi
}

# return help info when there are no parameters given

if [ $# -eq 0 ] ; then usage
        exit 0
fi

# options to use for the script

while [ -n "$1" ]; do

    case "$1" in
        -f|--full) 
		    check_root
		    for disk in $(lsblk -d | awk '$0 !~ /NAME/ {print "/dev/"$1}');
		    do smartctl -i ${disk} | grep "Logical Unit id:" | awk '{print "wwn-"$4}'
		    done ;
		    ;;
        -s|--small) 
		    check_root
		    for disk in $(lsblk -d | awk '$0 !~ /NAME/ {print "/dev/"$1}');
		    do smartctl -i ${disk} | grep "Logical Unit id:" | awk '{print $4}' | sed -r 's/^.{10}//' | tr '[a-z]' '[A-Z]'
		    done ;
		    ;;
        -h|--help) 
		    usage
		    ;;

        *)  
		    echo "For help: run ${name} -h";;
    esac
    shift
done
