#!/usr/bin/env python3
"""
    This script checks the amount and type of memory in the server
"""
import subprocess

MAX_DIFF = 7
MIN_TOTAL_MEM = 384
MIN_DIMM_COUNT = 16

def parsed_hwinfo():
    """ 
        Function that requests hwinfo 
        and parses it to a Python object
    """
    import os
    import json
    
    uid = os.getuid()
    data = ""

    if (uid):
        data = subprocess.run(
            "sudo lshw -c memory -json", 
            shell=True, 
            capture_output=True
        ).stdout
    else:
        data = subprocess.run(
            "lshw -c memory -json", 
            shell=True, 
            capture_output=True
        ).stdout

    return json.loads(data)

if __name__ == "__main__":
    # Select the object in the lshw-output that describes
    # system memory, and thus the DIMMs
    data = [
        obj for obj in
        parsed_hwinfo()
        if obj.get('description') == "System Memory"
    ][0]

    # Select the DIMMs that have a size > 0 (and != None)
    # (so skip the empty ones)
    DIMMs = [
        dimm for dimm in
        data.get('children')
        if dimm.get('size')
    ]

    # Get descriptions and product names 
    # and print
    print('DIMM Types:\n{0}'.format(
        '\n'.join(
            set([
                dimm.get('vendor') + ' => ' + dimm.get('product') 
                for dimm in DIMMs
            ])
        )
    ))
    print('---')

    # Print the DIMM descriptions to see which DDR version and DIMM speed
    print('DIMM Description:\n{0}'.format(
        '\n'.join(
             set([
                 dimm.get('description') for dimm in DIMMs
             ])
        )
    ))
    print('---')
    
    # Get the DIMM sizes, format as GiB
    # and print
    print('DIMM Sizes:\n{0}'.format(
        '\n'.join(
            set([
                '{:,.0f}'.format(
                    dimm.get('size')/float(1<<30)
                ) + " GiB" for dimm in DIMMs
            ])
        )
    ))
    print('---')
    
    # Get the count of DIMMs
    print('DIMM Count: {0}'.format(
        len(DIMMs)
    ))

    # Calculate the total memory of the DIMMs
    total_memory = sum(
        [
            int(dimm.get('size')) for dimm in DIMMs
        ])
    
    # Get the total memory of the DIMMs
    print('Total Memory: {0}'.format(
        '{:,.0f}'.format(
            total_memory / float(1<<30)
        ) + " GiB"
    ))

    # Get the total memory from 'free'
    free = int(subprocess.run(
        "free -b | awk '$1 == \"Mem:\" {print $2}'",
        shell=True,
        capture_output=True
    ).stdout)

    # Check if there's too much difference
    # between DIMMs and free
    diff = round((total_memory - free) / float(1<<30))
    if (diff > MAX_DIFF):
        print('Difference: {0} GiB'.format(
            diff
        ))

    # Check if there's enough DIMMs
    if (len(DIMMs) < MIN_DIMM_COUNT):
        print('WARNING: LESS THAN {0} DIMMs INSTALLED'.format(
            MIN_DIMM_COUNT
        ))
    
    # Check if there's enough memory
    if ((total_memory / float(1<<30)) < MIN_TOTAL_MEM):
        print('WARNING: LESS THAN {0} GiB INSTALLED'.format(
            MIN_TOTAL_MEM
        ))
