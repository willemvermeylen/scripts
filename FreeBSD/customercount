#!/bin/sh
#============================================================================
# HEADER
#============================================================================
# DESCRIPTION
#
#    This script checks if there are still customers on the server
#
#============================================================================
# IMPLEMENTATION
#
#    version         customercount 1.0
#    author          Willem Vermeylen
#
#============================================================================
#  HISTORY
#
#    2022/02/17 : wvermeylen : first initial release
# 
#============================================================================
# END_OF_HEADER
#============================================================================

PATH=/usr/gnu/bin:/usr/bin:/usr/sbin:/sbin:/opt/csw/bin:/usr/local/bin:/bin

# checking if the script is run as root

check_root()
{
    myuid=$(id -u)
    if [ $myuid != '0' ]; then
        echo "This script requires root privilege"
        exit 1
    fi
}
HOST=$(hostname)
if case $HOST in
        hv*)
                check_root
                echo "This is a VPS Hypervisor"
                COUNT=$(virsh list --all | egrep -v '^$' | awk 'NR>=3' | wc -l);;

        fe*|st*|bu*|fhbu*|sn*)

                ZPOOL=$(zpool status | egrep 'pool: (fe|st|bu|fhbu|sn)' | awk '{print $2}') 2> /dev/null
                BIG=$(zfs list | awk '$0~v' v="$ZPOOL/transip.nl-spool" | wc -l) 2> /dev/null

                case $ZPOOL in
                        
                        fe*)
                                check_root
                                echo "This is a Stack server";
                                COUNT=$(zfs list | awk '$0~v' v="$ZPOOL/" | wc -l);;

                        fhbu*)
                                check_root
                                echo "This is a Stack backup server";
                                COUNT=$(zfs list | awk '$0~v' v="$ZPOOL/" | wc -l);;

                        bu*)
                                check_root
                                echo "This is a VPS Backup server";
                                COUNT=$(zfs list | awk '$0~v' v="$ZPOOL/st[0-9]/transip.nl-vps/" | wc -l);;

                        st*)
                                check_root
                                echo "This is a VPS (Big)Storage server";
                                if [ $BIG = "0" ];
                                        then 
                                                COUNT=$(zfs list | awk '$0~v' v="$ZPOOL/transip.nl-vps/" | wc -l)
                                        else
                                                COUNT=$(zfs list | awk '$0~v' v="$ZPOOL/transip.nl-spool/" | wc -l)
                                fi;;
                esac

esac
        then 
                if [ $COUNT = "0" ];
                        then 
                                echo "Server is empty"
                        else
                                echo "$COUNT customers left on the server"
                fi
fi
