#!/bin/sh

PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin

set -e

# figure out which sas controller we have
SASIRCU=
for S in sas2ircu sas3ircu; do
    if ${S} LIST >/dev/null 2>&1; then
        SASIRCU=${S}
        break
    fi
done

if [ -z "${SASIRCU}" ]; then
    error "no suitable sas*ircu binary"
fi

error() {
  echo "Error: ${@}" >&2
  exit 1
}

get_adapters() {
  ${SASIRCU} LIST | awk '/^ +[0-9] +/ {print $1}'
}

get_all_positions() {
  for adapter in $(get_adapters); do
    ${SASIRCU} ${adapter} DISPLAY | \
      awk "BEGIN { RC=1 }
      /Enclosure # +: / { ENC=\$NF }
      /Slot # +: / { SLOT=\$NF }
      /Serial No +: / {RC=0; print \"${adapter}:\"ENC\":\"SLOT\"|\"\$NF; }
      END { exit RC }"
  done
}

wwn_for_serial() {
    local serial=$1
    geom disk list | \
		awk "/lunid: /{LUNID=\$NF}
		/ident: ${serial}/ {print LUNID}"
}

set_led() {
  local position=$1 status=$2 adapter encbay
  [ -z "${position}" -o -z "${status}" ] && error "set_led requires position and status parameter"

  echo ${position} | grep -qE '^[0-9]+:[0-9]+:[0-9]+$' || error "set_led requires position to be in form adapter:enclosure:bay"

  case ${status} in
    [Oo][Nn]|1) status=ON;;
    [Oo][Ff][Ff]|0) status=OFF;;
    *) error "set_led requires status to be ON or OFF";;
  esac

  adapter=$(echo ${position} | awk -F':' '{print $1}')
  encbay=$(echo ${position} | awk -F':' '{print $2":"$3}')

  ${SASIRCU} ${adapter} LOCATE ${encbay} ${status} >/dev/null 2>&1 || \
    error "failed setting ${encbay} on adapter ${adapter} to status ${status}"
}

main() {

  if [ $(id -u) -ne 0 ]; then
    error "This script needs to run as root"
  fi

  for position in $(get_all_positions); do
    pos=${position%%|*}
    serial=${position##*|}
    wwn=$(wwn_for_serial ${serial})
    [ -z "${wwn}" ] && continue

    clear

    echo "The LED of disk with WWN should now blink:"
    echo
    echo -n "     "
    echo $wwn | grep -o '.\{8\}$' | tr '[a-z]' '[A-Z]'
    echo
    echo "Press Enter to continue to the next disk..."

    set_led ${pos} on
    read FOOBAR
    set_led ${pos} off
  done
}

main
