#!/usr/bin/env bash

first="$1"

if [ -z ${first} ]; then
    echo "No hostname provided, please use --this to operate on this server"
    exit 1
elif [ "${first}" == "--this" ]; then
    hostname=$(hostname -f)
elif [ "${first}" == "--help" ]; then
    cat << EOF
  Generates a hash for a server, given the server's hostname or fqdn

  usage:
    $0 hostname
    $0 [--help|--this]

       --help	show this help
       --this	generate hash for this server, doesn't need a hostname parameter

EOF
    exit 0
else
    hostname=${first}
fi

hashpart=${hostname/.*/}
boards=(qwertyuiop asdfghjkl zxcvbnm 1234567890)
blen=${#boards[@]}
hlen=${#hashpart}

get_hash() {
    item=$1
    for (( i=0; i<${blen}; i++ )) ; do
        board=${boards[i]}
        cblen=${#board}
        prefix=${board%%$item*}
        ppos=${#prefix}
        [ ${ppos} -eq ${cblen} ] && continue
        echo ${board:$(((ppos + hlen) % cblen)):1}
    done
}

first=${hashpart:0:1}
last=${hashpart:$((hlen-1)):1}

first_hash=$(get_hash ${first})
last_hash=$(get_hash ${last})

echo "${first_hash}${last_hash}"
