#! /bin/bash

PATH=/usr/gnu/bin:/usr/bin:/usr/sbin:/sbin:/opt/csw/bin:/usr/local/bin:/bin

MIN_DIMM_COUNT=16
MIN_TOTAL_MEM=384
MAX_DIFF=7
DIMM_TYPE=$(sudo dmidecode -t memory | awk '$1 ~ "Type:" {print $2}' | grep -v "Unknown" | head -1);
DIMM_SIZE=$(sudo dmidecode -t memory | awk '$1 ~ "Size" {print $2}' | grep -v "No" | head -1 | cut -c1-2);
DIMM_COUNT=$(sudo dmidecode -t memory | awk '$1 ~ "Size" {print $2}' | grep -v "No" | wc -l );
dmi_mem_size=$(sudo dmidecode -t memory | awk '$1 ~ "Size" {sum += substr($2,0,3)} END {print sum}'); \
echo DIMM Size: "$DIMM_SIZE" GB;
echo DIMM Type: "$DIMM_TYPE";
echo DIMM amount: "$DIMM_COUNT";
echo Total: "$dmi_mem_size" GB;
proc_mem_size=$(free -g | awk '$1 == "Mem:" {print $2}' )
mem_diff=$((dmi_mem_size - proc_mem_size))
if [[ ${mem_diff} -gt ${MAX_DIFF} ]]; 
then
    echo Difference: "$mem_diff" GB; 
fi

if [[ ${DIMM_COUNT} -lt ${MIN_DIMM_COUNT} ]];
then
    echo WARNING: NOT ENOUGH DIMMS INSTALLED;
fi

if [[ ${dmi_mem_size} -lt ${MIN_TOTAL_MEM} ]];
then
    echo WARNING: LESS MEMORY THAN 384 GB;
fi

