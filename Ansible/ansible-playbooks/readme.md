## Using dynamic inventory

```bash
ansible-playbook playbooks/check_procs.yml -i vps-inventory.sh -f 20
```

For host groups you can choose from:

- vpsserver
- loadbalancer
- storageserver
- backupserver

You can combine these with pool names to target a specific pool, e.g. `vpsserver.pod5.ams4`.

## Tips and tricks

### Skip host-key checking

Yes, I know, this shouldn't be necessary.

```bash
export ANSIBLE_HOST_KEY_CHECKING=False
```
