#!/bin/sh
get_ip() {
  out=$(ip route show |grep default | grep enp0s31f6 | awk '{print $3}')
  echo $out
}

get_route() {
  out=$(ip route show |grep 10.3.0.0/16 | awk '{print $5}')
  echo $out
}

add_route() {
 route add -net 10.3.0.0/16 gw 10.3.30.1 enp0s31f6
}

# Make sure only root can run our script
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

set_route=$(get_route)
if ! [ -s $(get_ip) ]; then
  if [ -s $set_route ]; then
    add_route
  fi
fi
