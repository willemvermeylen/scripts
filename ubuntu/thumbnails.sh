#!/bin/bash

search_dir="$1"

for entry in "$search_dir"/*
do
# echo "$(basename "$entry")"

echo "Processing image $entry ..."
/usr/bin/convert -thumbnail 1024 "$entry" "$2"thumb."$(basename "$entry")"
done


